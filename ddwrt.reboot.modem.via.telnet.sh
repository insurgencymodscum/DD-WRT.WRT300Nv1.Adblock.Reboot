#!/bin/sh

###############################################################################

# sleep for amount specified in FILE. "echo 60 >/jffs/sleeptime" will sleep for 60 seconds

FILE=/jffs/sleeptime
if [ -f $FILE ]
then
    sleeptime=$(cat "$FILE");
    echo "sleeping: $sleeptime seconds"
    sleep $sleeptime;
else
    echo "$FILE does not exist, not sleeping";
fi

###############################################################################

HOST='192.168.1.1'
USER='admin'
PASSWD='admin'
CMD='reboot'

echo begin reboot

(
sleep 10
echo "$USER"
sleep 3
echo "$PASSWD"
sleep 3
echo "$CMD"
sleep 3
) | telnet "$HOST"

echo end reboot