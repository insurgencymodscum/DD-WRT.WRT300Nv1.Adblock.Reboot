#!/bin/sh

# Source: Copyright � 2014 Matthew Headlee <mmh@matthewheadlee.com> (http://matthewheadlee.com/).
# Modifications by INSMODSCUM. insurgencymod.blogspot.com
# Works on WRT300v1, DD-WRT v24-sp2 (08/12/10) mini - build 14929. 

###############################################################################

# sleep for amount specified in FILE. "echo 60 >/jffs/sleeptime" will sleep for 60 seconds

FILE=/jffs/sleeptime
if [ -f $FILE ]
then
    sleeptime=$(cat "$FILE");
    echo "sleeping: $sleeptime seconds"
    sleep $sleeptime;
else
    echo "$FILE does not exist, not sleeping";
fi

###############################################################################

# changing web-gui port from 80 to 88

if [[ -z "`ps | grep -v grep | grep "httpd -p 88"`" && `nvram get http_lanport` -ne 88 ]] ; then
echo "changing web-gui port from 80 to 88"
stopservice httpd
nvram set http_lanport=88
nvram set aviad_changed_nvram=1
startservice httpd
fi

# starting pixelserv19

if [[ -n "`ps | grep -v grep | grep /jffs/pixelserv19`" ]]
then
echo "pixelserv19 is already up"
else
echo "starting pixelserv19"
/jffs/pixelserv19 -p 80
fi

# Delete old hosts

echo "deleting old hosts"
rm -f /tmp/hosts
rm -f /tmp/hoststmp

# Download hosts files

echo "downloading hosts"
wget -qO /tmp/hoststmp \
'http://hosts-file.net/ad_servers.txt' \
'http://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext' \
'http://winhelp2002.mvps.org/hosts.txt' \
'http://someonewhocares.org/hosts/zero/hosts'
echo "downloading hosts done"

# Convert hosts text to the UNIX format. Strip comments, blank lines, and invalid characters. Replaces tabs/spaces with a single space, remove localhost entries.

echo "editing hosts"
sed -r -e "s/$(echo -en '\r')//g" \
-e '/^#/d' \
-e 's/#.*//g' \
-e 's/[^a-zA-Z0-9\.\_\t \-]//g' \
-e 's/(\t| )+/ /g' \
-e 's/^127\.0\.0\.1/0.0.0.0/' \
-e '/ localhost /d' \
-e '/^ *$/d' \
-e 's/0.0.0.0/192.168.2.1/g' \
-e "1s/^/$(hostname -i)$(hostname)\n127.0.0.1 localhost\n/" \
/tmp/hoststmp | \
sort -u > /tmp/hosts
echo "editing hosts done"

# Add "IP-ADDRESS HOSTNAME, 127.0.0.1 localhost" at the *beginning*

echo "adding DD-WRT and localhost entries"
sed -i "1s/^/$(hostname -i)$(hostname)\n127.0.0.1 localhost\n/" /tmp/hosts

# Tell dnsmasq to reread the updated hosts file. In theory /tmp/var/run/dnsmasq.pid should contain the PID of the only running dnsmasq process. kill -1 "$(cat /tmp/var/run/dnsmasq.pid)". Lets use ps instead.

ps | awk '/[d]nsmasq/ {print $1}' | xargs -n1 kill -1

# Cleanup

rm -f /tmp/hoststmp
stopservice dnsmasq
startservice dnsmasq

echo "number of lines: $(wc -l </tmp/hosts)"