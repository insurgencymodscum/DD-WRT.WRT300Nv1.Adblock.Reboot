###

Administration, Commands, Startup

Run adblocking script on every boot. If router loses power and reboots, we are still protected, otherwise would have to wait for the cron job to run. Adblocking script waits for a number of seconds specified in /jffs/sleeptime
/jffs/ddwrt.adblock.via.hosts.sh

###

Administration, Keep Alive, Schedule Reboot

Daily at 0605 hours. 

###

Administration, Management, Cron Jobs

#Reboot modem
0 6 * * * root /jffs/ddwrt.reboot.modem.via.telnet.sh
#Adblocking
10 6 * * * root /jffs/ddwrt.adblock.via.hosts.sh

###

Summary:

0600 reboot modem
0605 reboot dd-wrt
0606 adblock (dd-wrt startup script).
0610 adblock (cron job)